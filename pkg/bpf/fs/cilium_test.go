package fs

import (
	"fmt"
	"log"
	"math/rand"
	"sort"
	"testing"

	"github.com/cilium/ebpf"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dinoallo/sealos-networkmanager-library/pkg/bpf/common"
	testbpf "gitlab.com/dinoallo/sealos-networkmanager-library/test/bpf"
	testos "gitlab.com/dinoallo/sealos-networkmanager-library/test/os"
)

const (
	cmNamingTemplate = "cilium_calls_%05d"
)

func TestCepGetting(t *testing.T) {
	var NumOfCeps int = 10
	eids, closeMaps, err := setUpCiliumCMsForTesting(NumOfCeps)
	if closeMaps != nil {
		t.Cleanup(closeMaps)
	}
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	cbpffs := NewCiliumBPFFS(common.DefaultCiliumTCRoot)
	ceps, err := cbpffs.Ceps()
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	sort.Slice(ceps, func(i, j int) bool {
		return ceps[i] < ceps[j]
	})
	sort.Slice(eids, func(i, j int) bool {
		return eids[i] < eids[j]
	})
	t.Logf("eids: %v; ceps: %v", eids, ceps)
	for i := 0; i < NumOfCeps; i++ {
		assert.Equal(t, eids[i], ceps[i])
	}
}

func setUpCiliumCMsForTesting(NumOfCm int) ([]int64, func(), error) {
	var cms []*ebpf.Map
	var eids []int64
	var lastErr error = nil
	for i := 0; i < NumOfCm; i++ {
		eid := generateRandomEid()
		mapName := fmt.Sprintf(cmNamingTemplate, eid)
		m, err := testbpf.CreateAndPinMap(mapName, 50, common.DefaultCiliumTCRoot)
		if err != nil {
			lastErr = err
			log.Print(err)
			continue
		}
		eids = append(eids, eid)
		cms = append(cms, m)
	}
	closeMap := func() {
		for _, m := range cms {
			m.Unpin()
			m.Close()
		}
	}
	return eids, closeMap, lastErr
}

func generateRandomEid() int64 {
	return rand.Int63n(1e4) + 1
}

func TestMain(m *testing.M) {
	if !testos.CheckRoot() {
		log.Printf("this test must be run as root")
		return
	}
	m.Run()
}
