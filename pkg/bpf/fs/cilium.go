package fs

import (
	"log"
	"os"
	"regexp"
	"strconv"

	"errors"

	"gitlab.com/dinoallo/sealos-networkmanager-library/pkg/bpf/common"
)

const (
	cepRegex = `^cilium_calls_(\d+)$`
)

var (
	ErrCiliumTCRootNotExists   = errors.New("cilium tc root doesn't exist")
	ErrCiliumTCRootNotDir      = errors.New("cilium tc root is not a directory")
	ErrReadingFromCiliumTCRoot = errors.New("failed to read from cilium tc root")
)

type CiliumBPFFS_ interface {
	Ceps() ([]int64, error)
	CheckCiliumTCRoot() error
}

type CiliumBPFFS struct {
	ciliumTCRoot string
}

func NewCiliumBPFFS(ciliumTCRoot string) *CiliumBPFFS {
	if ciliumTCRoot == "" {
		ciliumTCRoot = common.DefaultCiliumTCRoot
	}
	return &CiliumBPFFS{
		ciliumTCRoot: ciliumTCRoot,
	}
}

func (f *CiliumBPFFS) CheckCiliumTCRoot() error {
	info, err := os.Stat(f.ciliumTCRoot)
	if os.IsNotExist(err) {
		return errors.Join(err, ErrCiliumTCRootNotExists)
	}
	if !info.IsDir() {
		return errors.Join(err, ErrCiliumTCRootNotDir)
	}
	return nil
}

func (f *CiliumBPFFS) Ceps() ([]int64, error) {
	if err := f.CheckCiliumTCRoot(); err != nil {
		return nil, err
	}
	cepDir := f.ciliumTCRoot
	entries, err := os.ReadDir(cepDir)
	if err != nil {
		return nil, errors.Join(err, ErrReadingFromCiliumTCRoot)
	}
	pattern := regexp.MustCompile(cepRegex)
	var eids []int64
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		filename := entry.Name()
		matches := pattern.FindStringSubmatch(filename)
		if matches != nil {
			numStr := matches[1]
			eid, err := strconv.ParseInt(numStr, 10, 64)
			if err != nil {
				log.Printf("Failed to convert %s to integer: %v\n", numStr, err)
				continue
			}
			eids = append(eids, eid)
		}
	}
	return eids, nil
}
