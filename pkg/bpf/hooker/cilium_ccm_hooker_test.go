package hooker

import (
	"fmt"
	"path/filepath"
	"testing"

	"github.com/cilium/ebpf"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dinoallo/sealos-networkmanager-library/pkg/bpf/common"
)

func TestMapValuePuttingDeleting(t *testing.T) {
	var eid int64 = 42
	mapName := fmt.Sprintf("test_map_%05d", 42)
	closeMap, err := setUpBPFMapForTesting(mapName)
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	t.Cleanup(closeMap)
	h := NewCiliumCCMHooker(eid)
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	mapPath := filepath.Join(common.DefaultCiliumTCRoot, mapName)
	err = h.putMapValue(egressHook, mapPath, uint32(0))
	assert.NoError(t, err)
	// clear the program
	err = h.delMapValue(mapPath, uint32(0))
	assert.NoError(t, err)
}

func setUpBPFMapForTesting(mapName string) (func(), error) {
	opts := ebpf.MapOptions{
		PinPath: common.DefaultCiliumTCRoot,
	}
	m, err := ebpf.NewMapWithOptions(&ebpf.MapSpec{
		Type:       ebpf.ProgramArray,
		KeySize:    4,
		ValueSize:  4,
		MaxEntries: 4,
		Pinning:    ebpf.PinByName,
		Name:       mapName,
	}, opts)
	if err != nil {
		return nil, err
	}
	return func() {
		m.Unpin()
		m.Close()
	}, nil
}
