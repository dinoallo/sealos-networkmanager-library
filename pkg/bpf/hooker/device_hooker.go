package hooker

import (
	"errors"
	"log"
	"net"

	"github.com/puzpuzpuz/xsync"
	"github.com/vishvananda/netlink"
	"golang.org/x/sys/unix"
)

type specialQdiscType int

const (
	qdiscRoot specialQdiscType = iota // qdiscEgress
	qdiscIngress
)

var (
	ErrFailedToAddFilter         = errors.New("failed to add the filter")
	ErrFailedToDeleteFilter      = errors.New("failed to delete the filter")
	ErrFailedToListFilter        = errors.New("failed to list all filters")
	ErrFailedToFindInterface     = errors.New("failed to find the interface")
	ErrFailedToAddClsactQdisc    = errors.New("failed to add the clsact qdisc")
	ErrFailedToDeleteClsactQdisc = errors.New("failed to delete the clsact qdisc")
	ErrFailedToListQdisc         = errors.New("failed to list the qdiscs")
	ErrClsactQdiscNotExists      = errors.New("the clsact qdisc doesn't exist")
	ErrInterfaceNotExists        = errors.New("the interface doesn't exist")
)

// TODO: maybe keep a copy of netlink.Link
type DeviceHooker struct {
	iface          *net.Interface
	rootFilters    *xsync.MapOf[string, *netlink.BpfFilter]
	ingressFilters *xsync.MapOf[string, *netlink.BpfFilter] //filterName -> filter
	managedQdisc   netlink.Qdisc
}

func NewDeviceHooker(ifaceName string, createClsActQdisc bool) (*DeviceHooker, error) {
	iface, err := interfaceByName(ifaceName)
	if err != nil {
		return nil, err
	}
	deviceHooker := &DeviceHooker{
		iface:          iface,
		ingressFilters: xsync.NewMapOf[*netlink.BpfFilter](),
		rootFilters:    xsync.NewMapOf[*netlink.BpfFilter](),
		managedQdisc:   nil,
	}
	if createClsActQdisc {
		clsact, err := addClsactQdisc(ifaceName)
		if err != nil {
			return nil, err
		}
		deviceHooker.managedQdisc = clsact
	}
	return deviceHooker, nil
}

func (dh *DeviceHooker) AddFilterToIngressQdisc(filterName string, progFD int) error {
	return dh.addFilter(filterName, progFD, qdiscIngress)
}

func (dh *DeviceHooker) AddFilterToEgressQdisc(filterName string, progFD int) error {
	return dh.addFilter(filterName, progFD, qdiscRoot)
}

func (dh *DeviceHooker) DelFilterFromIngressQdisc(filterName string) error {
	return dh.delFilter(filterName, qdiscIngress)
}

func (dh *DeviceHooker) DelFilterFromEgressQdisc(filterName string) error {
	return dh.delFilter(filterName, qdiscRoot)
}

func (dh *DeviceHooker) Close() error {
	var err error = nil
	delFilter := func(filterName string, filter *netlink.BpfFilter) bool {
		if filter == nil {
			return true
		}
		filterDelErr := netlink.FilterDel(filter)
		err = errors.Join(filterDelErr, ErrFailedToDeleteFilter)
		log.Printf("err deleting filter: %v", err)
		return true
	}
	dh.ingressFilters.Range(delFilter)
	dh.rootFilters.Range(delFilter)
	if dh.managedQdisc != nil {
		return dh.delClsactQdisc()
	}
	return err
}

func (dh *DeviceHooker) CheckClsActQdisc() error {
	ok, err := dh.checkClsActQdisc()
	if err != nil {
		return err
	}
	if !ok {
		return ErrClsactQdiscNotExists
	}
	return nil
}

func (dh *DeviceHooker) getFilters(sqt specialQdiscType) *xsync.MapOf[string, *netlink.BpfFilter] {
	switch sqt {
	case qdiscIngress:
		return dh.ingressFilters
	default:
		return dh.rootFilters
	}
}

func (dh *DeviceHooker) addFilter(filterName string, progFD int, sqt specialQdiscType) error {
	filters := dh.getFilters(sqt)
	clsActQdiscExists, err := dh.checkClsActQdisc()
	if err != nil {
		return err
	}
	if !clsActQdiscExists {
		return ErrClsactQdiscNotExists
	}
	attrs := dh.getAttrs(1, unix.ETH_P_ALL, sqt)
	bpfFilter := netlink.BpfFilter{
		FilterAttrs:  attrs,
		Name:         filterName,
		DirectAction: true,
	}
	// if there is a stale filter with the same name, remove it first
	if err := dh.removeStaleFilter(&bpfFilter); err != nil {
		return err
	}
	bpfFilter.Fd = progFD
	if err := netlink.FilterAdd(&bpfFilter); err != nil {
		return errors.Join(err, ErrFailedToAddFilter)
	}
	filters.Store(filterName, &bpfFilter)
	return nil
}

func (dh *DeviceHooker) removeStaleFilter(bpfFilter *netlink.BpfFilter) error {
	ifaceName := dh.iface.Name
	link, err := getLink(ifaceName)
	if err != nil {
		return err
	}
	existingFilters, err := netlink.FilterList(link, bpfFilter.Parent)
	if err != nil {
		return errors.Join(err, ErrFailedToListFilter)
	}
	for _, ef := range existingFilters {
		bf, ok := ef.(*netlink.BpfFilter)
		if !ok || bf == nil {
			continue
		}
		if bf.Name == bpfFilter.Name {
			if err := netlink.FilterDel(bpfFilter); err != nil {
				return errors.Join(err, ErrFailedToDeleteFilter)
			}
		}
	}
	return nil
}

func (dh *DeviceHooker) delFilter(filterName string, sqt specialQdiscType) error {
	filters := dh.getFilters(sqt)
	filter, added := filters.LoadAndDelete(filterName)
	if !added {
		return nil
	}
	clsActQdiscExists, err := dh.checkClsActQdisc()
	if err != nil {
		return err
	}
	if !clsActQdiscExists {
		return ErrClsactQdiscNotExists
	}

	if err := netlink.FilterDel(filter); err != nil {
		return errors.Join(err, ErrFailedToDeleteFilter)
	}
	return nil
}

func (dh *DeviceHooker) checkClsActQdisc() (bool, error) {
	link, err := getLink(dh.iface.Name)
	if err != nil {
		return false, err
	}
	qdiscs, err := safeQdiscList(link)
	if err != nil {
		return false, err
	}
	if len(qdiscs) < 1 {
		return false, nil
	}
	var found bool = false
	for _, qdisc := range qdiscs {
		if qdisc.Type() == "clsact" {
			found = true
			break
		}
	}
	return found, nil
}

func (dh *DeviceHooker) getAttrs(prio uint16, proto uint16, sqt specialQdiscType) netlink.FilterAttrs {
	var parent uint32
	switch sqt {
	case qdiscIngress:
		parent = netlink.HANDLE_MIN_INGRESS
	default:
		parent = netlink.HANDLE_MIN_EGRESS
	}
	handle := netlink.MakeHandle(prio, proto)
	attrs := netlink.FilterAttrs{
		LinkIndex: dh.iface.Index,
		Handle:    handle,
		Parent:    parent,
		Priority:  prio,
		Protocol:  proto,
	}
	return attrs
}

func (dh *DeviceHooker) delClsactQdisc() error {
	if dh.managedQdisc == nil {
		return nil
	}
	if err := netlink.QdiscDel(dh.managedQdisc); err != nil {
		return errors.Join(err, ErrFailedToDeleteClsactQdisc)
	}
	return nil
}

func addClsactQdisc(ifaceName string) (netlink.Qdisc, error) {
	link, err := getLink(ifaceName)
	if err != nil {
		return nil, err
	}
	qdisc := Clsact{
		QdiscAttrs: netlink.QdiscAttrs{
			LinkIndex: link.Attrs().Index,
			Handle:    netlink.MakeHandle(0xffff, 0),
			Parent:    netlink.HANDLE_CLSACT,
		},
	}
	if err := netlink.QdiscAdd(&qdisc); err != nil {
		return nil, errors.Join(err, ErrFailedToAddClsactQdisc)
	}
	return &qdisc, nil
}

func getLink(ifaceName string) (netlink.Link, error) {
	link, err := netlink.LinkByName(ifaceName)
	if err != nil {
		switch err.(type) {
		case netlink.LinkNotFoundError:
			return nil, errors.Join(err, ErrInterfaceNotExists)
		default:
			return nil, errors.Join(err, ErrFailedToFindInterface)
		}
	}
	return link, nil
}

func interfaceByName(ifaceName string) (*net.Interface, error) {
	link, err := getLink(ifaceName)
	if err != nil {
		return nil, err
	}
	return &net.Interface{
		Index: link.Attrs().Index,
		Name:  link.Attrs().Name,
		Flags: link.Attrs().Flags,
	}, nil
}

func safeQdiscList(link netlink.Link) ([]netlink.Qdisc, error) {
	qdiscs, err := netlink.QdiscList(link)
	if err != nil {
		return nil, errors.Join(err, ErrFailedToListQdisc)
	}
	result := []netlink.Qdisc{}
	for _, qdisc := range qdiscs {
		attrs := qdisc.Attrs()
		if attrs.Handle == netlink.HANDLE_NONE && attrs.Parent == netlink.HANDLE_ROOT {
			continue
		}
		result = append(result, qdisc)
	}
	return result, nil
}

type Clsact struct {
	netlink.QdiscAttrs
}

func (qdisc *Clsact) Attrs() *netlink.QdiscAttrs {
	return &qdisc.QdiscAttrs
}

func (qdisc *Clsact) Type() string {
	return "clsact"
}
