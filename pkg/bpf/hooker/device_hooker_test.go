package hooker

import (
	"log"
	"testing"

	"github.com/cilium/ebpf"
	"github.com/stretchr/testify/assert"
	"github.com/vishvananda/netlink"
	testbpf "gitlab.com/dinoallo/sealos-networkmanager-library/test/bpf"
	testos "gitlab.com/dinoallo/sealos-networkmanager-library/test/os"
)

var (
	ingressHook *ebpf.Program
	egressHook  *ebpf.Program
)

func TestFilterAddingAndDeleting(t *testing.T) {
	t.Run("with setting up clsact qdisc", func(t *testing.T) {
		tearDownNs, err := testos.SetupNetNs()
		if !assert.NoError(t, err) {
			assert.FailNow(t, "failed to set up netns", err)
		}
		defer tearDownNs()
		ifaceName := "ti421"
		err = setupDummyInterface(ifaceName, false)
		if !assert.NoError(t, err) {
			assert.FailNow(t, "a dummy interface cannot be set up", err)
		}
		devHooker, err := NewDeviceHooker(ifaceName, true)
		if !assert.NoError(t, err) {
			assert.FailNow(t, "a device hooker cannot be created", err)
		}
		defer devHooker.Close()
		// ingress
		filterName := "ingress_filter"
		err = devHooker.AddFilterToIngressQdisc(filterName, ingressHook.FD())
		assert.NoError(t, err)
		err = devHooker.DelFilterFromIngressQdisc(filterName)
		assert.NoError(t, err)
		// egress
		filterName = "egress_filter"
		err = devHooker.AddFilterToEgressQdisc(filterName, egressHook.FD())
		assert.NoError(t, err)
		err = devHooker.DelFilterFromEgressQdisc(filterName)
		assert.NoError(t, err)
	})
	t.Run("without setting up clsact qdisc", func(t *testing.T) {
		tearDownNs, err := testos.SetupNetNs()
		if !assert.NoError(t, err) {
			assert.FailNow(t, "failed to set up netns", err)
		}
		defer tearDownNs()
		ifaceName := "ti422"
		err = setupDummyInterface(ifaceName, true)
		if !assert.NoError(t, err) {
			assert.FailNow(t, "a dummy interface cannot be set up", err)
		}
		devHooker, err := NewDeviceHooker(ifaceName, false)
		if !assert.NoError(t, err) {
			assert.FailNow(t, "a device hooker cannot be created", err)
		}
		defer devHooker.Close()
		// ingress
		filterName := "ingress_filter"
		err = devHooker.AddFilterToIngressQdisc(filterName, ingressHook.FD())
		assert.NoError(t, err)
		err = devHooker.DelFilterFromIngressQdisc(filterName)
		assert.NoError(t, err)
		// egress
		filterName = "egress_filter"
		err = devHooker.AddFilterToEgressQdisc(filterName, egressHook.FD())
		assert.NoError(t, err)
		err = devHooker.DelFilterFromEgressQdisc(filterName)
		assert.NoError(t, err)
	})
}

func setupDummyInterface(ifaceName string, setUpClsAct bool) error {
	dummy := netlink.Dummy{
		LinkAttrs: netlink.LinkAttrs{
			Name: ifaceName,
		},
	}
	if err := netlink.LinkAdd(&dummy); err != nil {
		log.Printf("failed to add link")
		return err
	}
	link, err := netlink.LinkByName(ifaceName)
	if err != nil {
		log.Printf("failed to get link")
		return err
	}
	if err := netlink.LinkSetUp(link); err != nil {
		log.Printf("failed to set link up")
		return err
	}
	if setUpClsAct {
		clsact := &Clsact{
			QdiscAttrs: netlink.QdiscAttrs{
				LinkIndex: link.Attrs().Index,
				Handle:    netlink.MakeHandle(0xffff, 0),
				Parent:    netlink.HANDLE_CLSACT,
			},
		}
		if err := netlink.QdiscAdd(clsact); err != nil {
			return err
		}
	}
	return nil
}

func TestMain(m *testing.M) {
	if !testos.CheckRoot() {
		log.Printf("this test must be run as root")
		return
	}
	prog, err := testbpf.LoadDummyBPFProgram()
	if err != nil {
		log.Print(err)
		return
	}
	ingressHook = prog
	egressHook = prog
	m.Run()
	// os.Exit(code)
}
