package hooker

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/cilium/ebpf"
	"gitlab.com/dinoallo/sealos-networkmanager-library/pkg/bpf/common"
)

const (
	ciliumCCMapTemplate = "cilium_calls_custom_%05d"

	// https://fossies.org/linux/cilium/bpf/custom/README.rst
	ciliumCCMapKeyForIngressV4 = 0
	ciliumCCMapKeyForEgressV4  = 1
	ciliumCCMapKeyForIngressV6 = 2
	ciliumCCMapKeyForEgressV6  = 3
)

// hooker for cilium custom call map
type CiliumCCMHooker struct {
	eid int64
}

func NewCiliumCCMHooker(eid int64) *CiliumCCMHooker {
	return &CiliumCCMHooker{
		eid: eid,
	}
}

func (h *CiliumCCMHooker) AttachV4IngressHook(hook *ebpf.Program) error {
	return h.attachHook(hook, ciliumCCMapKeyForIngressV4)
}

func (h *CiliumCCMHooker) AttachV4EgressHook(hook *ebpf.Program) error {
	return h.attachHook(hook, ciliumCCMapKeyForEgressV4)
}

func (h *CiliumCCMHooker) AttachV6IngressHook(hook *ebpf.Program) error {
	return h.attachHook(hook, ciliumCCMapKeyForIngressV6)
}

func (h *CiliumCCMHooker) AttachV6EgressHook(hook *ebpf.Program) error {
	return h.attachHook(hook, ciliumCCMapKeyForEgressV6)
}

func (h *CiliumCCMHooker) DetachV4IngressHook() error {
	return h.detachHook(ciliumCCMapKeyForIngressV4)
}

func (h *CiliumCCMHooker) DetachV4EgressHook() error {
	return h.detachHook(ciliumCCMapKeyForEgressV4)
}

func (h *CiliumCCMHooker) DetachV6IngressHook() error {
	return h.detachHook(ciliumCCMapKeyForIngressV6)
}

func (h *CiliumCCMHooker) DetachV6EgressHook() error {
	return h.detachHook(ciliumCCMapKeyForEgressV6)
}

func (h *CiliumCCMHooker) attachHook(hook *ebpf.Program, customMapKey uint32) error {
	ccmFile := fmt.Sprintf(ciliumCCMapTemplate, h.eid)
	ccmPath := filepath.Join(common.DefaultCiliumTCRoot, ccmFile)
	return h.putMapValue(hook, ccmPath, customMapKey)
}

func (h *CiliumCCMHooker) detachHook(customMapKey uint32) error {
	ccmFile := fmt.Sprintf(ciliumCCMapTemplate, h.eid)
	ccmPath := filepath.Join(common.DefaultCiliumTCRoot, ccmFile)
	return h.delMapValue(ccmPath, customMapKey)
}

func (h *CiliumCCMHooker) putMapValue(hook *ebpf.Program, ccmPath string, customMapKey uint32) error {
	ccm, err := checkAndLoadPinnedMap(ccmPath)
	if err != nil {
		return err
	}
	defer ccm.Close()
	if err := ccm.Put(customMapKey, hook); err != nil {
		return errors.Join(err, ErrUpdatingCiliumCCM)
	}
	return nil
}

func (h *CiliumCCMHooker) delMapValue(ccmPath string, customMapKey uint32) error {
	ccm, err := checkAndLoadPinnedMap(ccmPath)
	if err != nil {
		return err
	}
	defer ccm.Close()
	if err := ccm.Delete(customMapKey); err != nil {
		return errors.Join(err, ErrUpdatingCiliumCCM)
	}
	return nil
}

func checkAndLoadPinnedMap(ccmPath string) (*ebpf.Map, error) {
	if _, err := os.Stat(ccmPath); errors.Is(err, os.ErrNotExist) {
		return nil, errors.Join(err, ErrCiliumCCMNotExists)
	} else if err != nil {
		return nil, errors.Join(err, ErrStatingCiliumCCM)
	}
	ccm, err := ebpf.LoadPinnedMap(ccmPath, nil)
	if err != nil {
		return nil, errors.Join(err, ErrLoadingCiliumCCM)
	}
	return ccm, nil
}
