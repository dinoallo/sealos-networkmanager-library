package os

import (
	"os"
	"runtime"

	"github.com/vishvananda/netns"
)

// CheckRoot checks if the user running the test is root
func CheckRoot() bool {
	if os.Getuid() != 0 {
		return false
	}
	return true
}

// SetupNetNs sets up an independent network namespace and sets
// current os thread's netns to it.
// Calling this function in a goroutine results in locking that
// goroutine to the os thread, and it won't change the netns of
// other goroutines
func SetupNetNs() (func(), error) {
	runtime.LockOSThread()
	ns, err := netns.New()
	if err != nil {
		runtime.UnlockOSThread()
		return nil, err
	}
	return func() {
		ns.Close()
		runtime.UnlockOSThread()
	}, nil
}
