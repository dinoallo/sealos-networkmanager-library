package bpf

import (
	"errors"

	"github.com/cilium/ebpf"
	"github.com/cilium/ebpf/asm"
)

var (
	ErrLoadingBPFProgram = errors.New("failed to load ebpf program")
)

func LoadDummyBPFProgram() (*ebpf.Program, error) {
	spec := ebpf.ProgramSpec{
		Name: "test",
		Type: ebpf.SchedCLS,
		Instructions: asm.Instructions{
			// Set exit code to 0
			asm.Mov.Imm(asm.R0, 0),
			asm.Return(),
		},
		License: "GPL",
	}
	// Load the eBPF program into the kernel.
	prog, err := ebpf.NewProgram(&spec)
	if err != nil {
		return nil, errors.Join(err, ErrLoadingBPFProgram)
	}
	return prog, nil
}

func CreateAndPinMap(mapName string, maxEntries uint32, pinnedRoot string) (*ebpf.Map, error) {
	opts := ebpf.MapOptions{
		PinPath: pinnedRoot,
	}
	m, err := ebpf.NewMapWithOptions(&ebpf.MapSpec{
		Type:       ebpf.ProgramArray,
		KeySize:    4,
		ValueSize:  4,
		MaxEntries: maxEntries,
		Pinning:    ebpf.PinByName,
		Name:       mapName,
	}, opts)
	if err != nil {
		return nil, err
	}
	return m, nil

}
